#include "ArzuParser.h"
#include <fstream>
#include <iostream>
#include <string>
using namespace std;

int main() {
	Memory userRAM;
	ifstream inFile("test.arzu");
	ArzuParser parser;
	parser.parse(inFile, userRAM);
	inFile.close();
}
