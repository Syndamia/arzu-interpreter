#include "MemoryData.h"
#include <cstring>

MemoryData::Type MemoryData::get_type() const {
	return this->type;
}

MemoryData::MemoryData() : type(TNone) {}

MemoryData* MemoryData::clone() {
	return new MemoryData(*this);
}

Name::Name() : value() {
	this->type = TName;
}

Name::Name(string& str) : value(str) {
	this->type = TName;
}

MemoryData* Name::clone() {
	return new Name(*this);
}

Int::Int(int num) : value(num) {
	this->type = TInt;
}

MemoryData* Int::clone() {
	return new Int(*this);
}

Function::Function(list<string> argNames, int scopeStart, int scopeEnd) : argumentNames(argNames), scopeStart(scopeStart), scopeEnd(scopeEnd) {
	this->type = TFunc;
}

MemoryData* Function::clone() {
	return new Function(*this);
}
