#ifndef ARZU_INTERPRETER_PARSER
#define ARZU_INTERPRETER_PARSER

#include <list>
#include <deque>
#include <string>
#include <fstream>
#include "Memory.h"
using namespace std;

struct Instruction {
	string name;
	unsigned argc;
	void (*exec)(Memory&);

	Instruction(const char* str, unsigned argc, void (*exec)(Memory&));
};

struct Atom {
	bool (*isAtomChar)(char);
	MemoryData* (*absorb)(ifstream&);

	Atom(bool (*isAtomChar)(char), MemoryData* (*absorb)(ifstream&));
};

class PNParser {
	deque<Instruction> instr;
	deque<Atom> atoms;

	void evaluateFunction(Memory& mem, Function* func, ifstream& inFile);
	void pushToWork(Memory& mem, MemoryData* data, ifstream& inFile);
	void parseScope(ifstream& inFile, Memory& mem, int start, int end);
public:
	PNParser() = default;
	void addInstr(Instruction);
	void addAtom(Atom);
	void parse(ifstream&, Memory&);
};

#endif
