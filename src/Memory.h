#ifndef ARZU_INTERPRETER_MEMORY
#define ARZU_INTERPRETER_MEMORY

#include "MemoryData.h"
#include <list>
#include <stack>
using namespace std;

struct Memory {
	list<MemoryData*> vars;
	stack<MemoryData*> work;
	stack<unsigned int> scopeVars;
};

#endif
