#ifndef ARZU_INTERPRETER_MEMORY_DATA
#define ARZU_INTERPRETER_MEMORY_DATA

#include <list>
#include <string>
using namespace std;

class MemoryData {
public:
	enum Type {
		TNone,
		TName,
		TFunc,
		TInt,
	};

protected:
	Type type;

public:

	Type get_type() const;
	virtual MemoryData* clone();

	MemoryData();
	virtual ~MemoryData() = default;
};

struct Name : public MemoryData {
	string value;
	Name();
	Name(string&);

	MemoryData* clone() override;
};

struct Int : public MemoryData {
	int value;
	Int(int);

	MemoryData* clone() override;
};

struct Function : public MemoryData {
	list<string> argumentNames;
	int scopeStart;
	int scopeEnd;
	Function(list<string>, int, int);

	MemoryData* clone() override;
};

#endif
